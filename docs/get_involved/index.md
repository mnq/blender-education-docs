---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Get involved

Join the Blender Education Community to connect with Blender teachers around the world.<br><br>
The community welcomes new ideas and initiatives dedicated to teaching Blender and appreciates support for ongoing collaborative efforts, such as maintaining the website or further developing the Blender Badges project.
You can also join the [Blender Education chat](https://chat.blender.org/#/room/#blender-education:blender.org).

<div class="grid cards" markdown>

- **Contributors**

  ---
Many teachers and students contribute in some way to the Blender Education initiatives. Some teachers and students are actively participating in the Blender Education community projects. Below is a list of current active contributors.
</div>

<div class="grid cards" markdown>


- **Catina Yiu**

  ---
[Krystal Institute](https://www.krystal.institute/)</br>
Hong Kong

- **Edouard Simon**

  ---
[mediencollege Berlin](https://mediencollege.berlin)</br>
Berlin

- **Federico Fiore**

  ---
[NYP School of Design & Media](https://www.nyp.edu.sg/student/study/schools/design-media)</br>
Singapore

- **Fons Artois**

  ---



- **Matte Malmlöf**

  ---
[AFRY](https://afry.com/en) and [YRGO](https://www.yrgo.se/)</br>
Gothenburg

- **Monique Dewanchand**

  ---
[B3d101](https://www.b3d101.org/) and [Metis Coderclass](https://hetmml.nl/vakken/coderclass/)</br>
Amsterdam

- **Philipp A. Opitz**

  ---
[Graphic & UX Designer](https://www.instagram.com/philipp.a.opitz?igsh=MWphMzZwanVxMm5hdQ%3D%3D&utm_source=qr)</br>
Berlin

- **Peter Kemp**

  ---
[3Dami](https://3dami.org/)</br>
King's College London

- **Timothy Tan**

  ---
[Krystal Institute](https://www.krystal.institute/)</br>
Hong Kong

</div>
