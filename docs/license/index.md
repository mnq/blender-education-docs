---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# License

Blender itself is released under the [GNU General Public License](https://www.blender.org/about/license/).

Except where otherwise noted, the content of the Blender Education Documentation is available under a
[CC0 4.0](https://creativecommons.org/public-domain/cc0/),
or any later version.

If you have questions about the license, feel free to contact one of the members of the Blender Education Community.