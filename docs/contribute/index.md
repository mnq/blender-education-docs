---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Contribute

This project is still in development.

There are many ways to contribute to this project, for example,

  Proofreading

  Create badges
  
  Create tutorials
  
  Translations

If you would like to contribute, join the [Blender Education chat](https://chat.blender.org/#/room/#blender-education:blender.org) and contact one of the contributors.


