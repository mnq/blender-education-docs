---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Flower
## Rig a 3-bones flower

### Task

- Rig a 3-bones flower. Add controllers to the flower rig.

- Flower

  - Create a bone.
  - Extrude new bones.
  - Edit bones position to match geometry.
  - Rename bones.
  - Assign a custom color.
  - Constrain "armature" the geometry to the bones.
  - Edit paint weight of flower geometry.
  - Pose the bones to bend the flower.

- Add controllers to the flower rig

  - Create control bone (duplicate originals) & rename them.
  - Unparent them from original link.
  - Parent them to each other in a hierarchical way (lower control the one above).
  - Create empty with circle shape and hide it.
  - Assign custom shape of empty.
  - Assign custom color.
  - Assign controllers to new bone collection.
  - Pose the controllers to bend the flower.

Example

![](../../../resources/img/rig-flower-basic.png){align=left width="200"}
![](../../../resources/img/rig-flower-colour.png){align=left width="200"}
![](../../../resources/img/rig-flower-control.png){align=left width="200"}