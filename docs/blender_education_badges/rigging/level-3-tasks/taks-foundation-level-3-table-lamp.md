---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Table lamp
## Rig a 3-bones IK table lamp

### Task

- Rig a simple 3-bones IK table lamp. Then add a master controller.

  - Create deformation bones & position to match geometry.
  - Edit and rename bones.
  - Constrain "child of" the geometry to the bones (set inverse).
  - Create IK control & IK pole bones.
  - Assign custom shape & color to IK controllers.
  - Assign controllers to the new bone collection.
  - Create IK bone constraints.
  - Create Copy Rotation constraint for the IK ctrl to control orientation of lamp shade.
  - Pose the IK controllers to bend the lamp.

- Add a Master controller to previous rig setup

  - Create a Master control bone (custom shape + color).
  - Parent first deform bone to Master controller and constrain "child of" both IK controllers to Master controller.


Example

![](../../../resources/img/rig-ik-lamp.png){align=left width="200"}
![](../../../resources/img/rig-ik-lamp-controller.png){align=left width="200"}