---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Lamp
## Rig a lamp

### Task

- Rig a simple 2-bones table lamp.

  - Create a bone.
  - Extrude 2nd bone.
  - Edit bones position to match geometry.
  - Rename bones.
  - Constrain "child of" the geometry to the bones (set inverse).
  - Pose the bones to bend the lamp.

Example

![](../../../resources/img/rig-lamp.png){align=left width="200"}