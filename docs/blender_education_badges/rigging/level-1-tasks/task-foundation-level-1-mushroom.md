---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Mushroom
## Rig a mushroom

### Task

- Rig a simple 2-bones mushroom.

  - Create a bone.
  - Extrude 2nd bone.
  - Edit bones position to match geometry.
  - Rename bones.
  - Constrain "armature" the geometry to the bones.
  - Pose the bones to bend the mushroom.

Example

![](../../../resources/img/rig-mushroom.png){align=left width="200"}