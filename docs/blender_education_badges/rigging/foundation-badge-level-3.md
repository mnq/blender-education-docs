---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Rigging Foundation Level 3

<div class="grid cards" markdown>

- **Inverse Kinematics & Master Control Setup**

   ---
  ![](../../resources/img/badges/Rigging%20Level%203.png){align=left width="100"}
  Inverse kinematic (IK) allows for a more natural interaction with the rig. Here we will explore the basic IK setup in Blender, inclusive of pole target, dual constraint setup and master controller.

</div>

<div class="grid cards" markdown>

 - **What a student will learn**

   ---

   IK Control Bones

   - Create bones as controllers (IK & Pole)
   - IK constraint
   - IK Chain length
   - IK Pole Target/Angle
   - Copy Rotation constraint


   Dual constraints setup

   - IK + Rotation-only

   Master Control Setup

   - Create bone as main controller or the whole rig

   Lock Controllers' transformations

   - Location / Rotation / Scale

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - CTRL+Tab: pose mode

   - F3: Scale B-Bone
   - rmb: subdivide

   - SHIFT+d: duplicate bone
   - ALT+P: clear parent
   - CTRP+p: parent keep offset

   - SHIFT+I: add IK to target bone

   - ALT+g: clear translations
   - ALT+r: clear rotations


</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can**

   - Create IK controlled 3-bones rigs
   - Assign pole to IK constraints
   - Control IK chain length
   - Create dual constraints rigs

   **Knows**

   - Inverse Kinematic functioning
   - Pole vector purpose
   - Assign two constraints to the same controller/bone

</div>

<div class="grid cards" markdown>

- **Example tasks for Rigging Foundation Level 3**

   ---

   - [Rig a table lamp](./level-3-tasks/taks-foundation-level-3-table-lamp.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Rigging Foundation Level 3 Badge](../../resources/img/badges/Rigging%20Level%203.png)
     
</div>