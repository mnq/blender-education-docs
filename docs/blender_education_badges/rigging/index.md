---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Rigging Badges

Rigging allows the artist to create underlying structures and controls to animate 3D models. Blender offers an impressive set of rigging tools including skeleton and automatic skinning, easy weight painting, constraints, bone layers and colored groups for organization.


<div class="grid cards" markdown>

- **Foundation badge Level 1**

  ---
  ![](../../resources/img/badges/Rigging%20Level%201.png){align=left width="100"}
  Creating Armatures</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-1.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 2**

  ---
  ![](../../resources/img/badges/Rigging%20Level%202.png){align=left width="100"}
  Forward Kinematics Control Bones</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-2.md)  

</div>
<div class="grid cards" markdown>

- **Foundation badge Level 3**

  ---
  ![](../../resources/img/badges/Rigging%20Level%203.png){align=left width="100"}
  Inverse Kinematics & Master Control Setup</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-3.md)  

</div>
