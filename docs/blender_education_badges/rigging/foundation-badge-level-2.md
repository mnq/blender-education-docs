---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Rigging Foundation Level 2

<div class="grid cards" markdown>

- **Forward Kinematics, Control Bones**

   ---
   ![](../../resources/img/badges/Rigging%20Level%202.png){align=left width="100"}
   Kicking it up a notch, we will look at customizing bones appearance and shape, control their influence on the geometry using weight paint and creating forward kinematic controllers. This will give the artist a finer level of control over their rig.

</div>

<div class="grid cards" markdown>

 - **What a student will learn**

   ---

   Creating Armatures

   - Changing bones shape (viewport display)
   - Change bones color

   Editing Armatures

   - Scale BBone
   - Subdivide bone

   Editing WeightPaint

   - Draw
   - Weight
   - Blur

   FK Control Bones

   - Create bones as controllers
   - Parent/unparent between bones
   - Custom shape of control bones
   - Bones collections

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - CTRL+Tab: pose mode
   - F3: Scale B-Bone
   - rmb: subdivide
   - SHIFT+d: duplicate bone
   - ALT+P: clear parent
   - CTRL+p: parent keep offset

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can**

   - Change bone shape & color
   - Subdivide bones
   - Edit paintweight
   - Constraint bones

   **Knows**

   - Customize shapes & color of bones
   - Adjust the weight paint to improve the geometry deformation
   - Create basic armature  controllers


</div>

<div class="grid cards" markdown>

- **Example tasks for Rigging Foundation Level 2**

   ---

   - [Rig a flower](./level-2-tasks/task-foundation-level-2-flower.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Rigging Foundation Level 2 Badge](../../resources/img/badges/Rigging%20Level%202.png)
     
</div>