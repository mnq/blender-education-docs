---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Rigging Foundation Level 1


<div class="grid cards" markdown>

- **Creating Armatures**

   ---
  ![](../../resources/img/badges/Rigging%20Level%201.png){align=left width="100"}
  Learn how to start rigging in Blender here. From creating armatures to constrain geometry, you will be able to design your first rig and pose simple 3D models.


</div>

<div class="grid cards" markdown>

 - **What a student will learn**

  ---

  Creating Armatures

  - Creating single bone
  - Structure (tip, body. root)
  - Rename bones

  Editing Armatures

  - Edit mode
  - Extruding new bones

  Constraining Geometry

  - Child of
  - Armature

  Animating Armatures

   - Pose mode

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - Shift+a: add menu (Armature)

   - Tab: edit mode
     - e: extrude new bone
     - g/r: move/rotate bone components


   - CTRL+p constraint menu


</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can**

   - Create bones
   - Rename bones
   - Extrude bones
   - Constraint "child of" and "armature"
   - Pose the bones

   **Knows**

   - What bones are
   - How to edit basic shape
   - Constraint to a geometry
   - Pose bones


</div>


<div class="grid cards" markdown>

- **Example tasks for Rigging Foundation Level 1**

   ---

   - [Rig a lamp](./level-1-tasks/task-foundation-level-1-lamp.md): Rig a simple 2-bones table lamp.
   - [Rig a mushroom](./level-1-tasks/task-foundation-level-1-mushroom.md): Rig a simple 2-bones mushroom.

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Rigging Foundation Level 1 Badge](../../resources/img/badges/Rigging%20Level%201.png)
     
</div>