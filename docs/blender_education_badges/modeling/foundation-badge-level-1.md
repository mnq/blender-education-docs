---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Modeling Foundation Level 1

<div class="grid cards" markdown>

- **Editing Objects**

   ---
   ![](../../resources/img/badges/Modelling%20Level%201.png){align=left width="100"}
This badge focuses on teaching essential Blender skills, including the manipulation of mesh objects and the use of various tools and shortcuts. You'll learn to construct models from primitive shapes while mastering viewport navigation and scene composition.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---
   Primitive (Mesh) Objects

   - Cube
   - Plane
   - Cone
   - Cylinder
   - UV Spheres
   - Suzanne

   Manipulations of objects and meshes

   - Add
   - Delete
   - Move
   - Rotate
   - Scale

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   Viewport

   - Left click: Select the object
   - Press MMB: Rotate viewport
   - Scrolling MMB: Scale up/down viewport
   - Camera widget
   - Viewport Shading
   - Undo

   Menu

   - Add menu: Add (Mesh) object
   - File menu: Save file

   Tools in Object mode

   - Move tool
   - Scale tool
   - Rotate tool

   Shortcuts

   - X key to delete the selected object


</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can**

   - Add and delete objects
   - Move, rotate and scale objects along x, y and z axes
   - Rotate the viewport to look at different parts of the scene
   - Look through the camera lens
   - Save blender file

   **Knows**

   - A scene is made up of objects, lights and camera
   - Basics of object transformation and manipulation
   - Shading modes
   - Everything is editable!

</div>

<div class="grid cards" markdown>

- **Example tasks for Modeling Foundation Level 1**

   ---

   - [Party Monkey](./level-1-tasks/task-foundation-level-1-party_monkey.md)
   - [Tree of Cubes](./level-1-tasks/task-foundation-level-1-tree.md)
   - [Snowman](./level-1-tasks/task-foundation-level-1-snowman.md)
   - [Bunny](./level-1-tasks/task-foundation-level1-bunny.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Modeling Foundation Level 1 Badge](../../resources/img/badges/Modelling%20Level%201.png)
     
</div>