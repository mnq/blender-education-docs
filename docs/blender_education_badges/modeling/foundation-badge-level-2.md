---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Modeling Foundation Level 2

<div class="grid cards" markdown>

- **The Basics of Object Manipulation**

   ---
  ![](../../resources/img/badges/Modelling%20Level%202.png){align=left width="100"}
  This badge goes deeper into working with objects, covering parenting, duplicating, and extruding, as well as manipulating vertices, edges, and faces in edit mode. It also includes coloring objects, and takes a closer look at creating models, navigating the interface, and applying materials.

</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Object Manipulation

   - Object and Edit mode
   - Select modes: Vertex select, Edge select, Face select
   - Move edges, faces, vertices
   - Duplicate
   - Parent
   - Extrude

   Material

   - Add material to object
   - Add and assign material to faces

   Rendering

   - Add lighting
   - Add and set up camera

   Properties

   - Object properties
   - Material properties

   Others

   - Open or close a file
   - Keyboard shortcut for basic operations

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   Menu

   - Load menu to find .blend file
   - Object menu: Duplicate and Join
   - Select modes
   - Object interaction mode: Object - Edit mode
   - View menu: Navigation - Fly mode

   Outliner

   - Organize object hierarchy
   - Select multiple objects
   - Use the outliner to parent objects
   
   Viewport

   - Move object around scene
   - Multiple select
   - Navigation widget: Rotate viewport

   Object properties

   - Use object properties to give object names

   Material properties

   - Diffuse color
   - Add new material
   - Give material a name
   - Change color of a material
   - Reuse material

   Tools in Edit mode

   - Move 
   - Scale 
   - Rotate 
   - Extrude
   - Use CTRL to limit extrusion

   Shortcuts

   - G: Grab or Move
   - R: Rotate
   - S: Scale
   - SHIFT + D: Duplicate
   - CTRL + P: Parenting
   - E: Extrude
   - CTRL + P: Join
   - Press TAB: Switch between Object and Edit mode

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Add materials to objects
   - Manipulate objects, faces, edges and vertices
   - Multiple select and Edit
   - Organize and hierarchy of objects
   - Name objects and materials
   - Duplicate, join and parent objects

   **Knows:**

   - Why naming objects and materials is important
   - Diffuse material
   - Reuse materials
   - Decomposition: parent or join objects
   - Object hierarchy and parenting: Utilizing the Outliner to organize and manage objects within the scene.
   - Modeling Techniques: Basics of object transformation, face or edge selection and extrusion.
   - Navigation Tools: Applying Camera Fly Mode for immersive view.

</div>

<div class="grid cards" markdown>

- **Example tasks for Modeling Foundation Level 2**

   ---

   - [Colour the snowman](./level-2-tasks/task-foundation-level-2-colour-the-snowman.md)
   - [Rocket Rabbit](./level-2-tasks/task-foundation-level-2-colour-the-rabbit.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Modeling Foundation Level 2 Badge](../../resources/img/badges/Modelling%20Level%202.png)
     
</div>