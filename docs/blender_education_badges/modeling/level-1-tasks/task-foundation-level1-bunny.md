---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Bunny
## Create a bunny

### Task

- Create a bunny from several UV Spheres.

  - Add 2 UV spheres.
  - Scale the UV spheres and place them on top of each other, creating a head and a body.
  - Add 2 UV spheres.
  - Scale the UV spheres and create the hind legs.
  - Add 2 UV spheres.
  - Scale the UV spheres and create the front legs.
  - Add 2 UV spheres.
  - Scale the UV spheres and create the ears.
  - Add a UV sphere.
  - Scale the UV sphere and create a tail.
  - Add 2 UV spheres.
  - Scale the UV sphere and create the eyes.
  - Add a UV sphere.
  - Scale the UV sphere and create a nose.