---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Snowman
## Snowman with hat and scarf

### Task

- Create a snowman from several UV Spheres.

  - Add 3 UV spheres.
  - Scale the UV spheres and place them on top of each other, creating a head, body and bottom.
  - Add 3 more UV spheres and scale them creating eyes for the snowman.
  - Add a cone and scale it creating a nose for the snowman.
  - Add more UV spheres and scale them creating buttons for the snowman.
  - Add a hat and a scarf.