---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Party Monkey
## Party monkey with a party hat

### Task

- Create a party monkey with a hat.

  - Add Suzanne.
  - Add a cone or cylinder on top of Suzanne.
  - Make sure Suzanne's face is properly lit.
  - Render the image.