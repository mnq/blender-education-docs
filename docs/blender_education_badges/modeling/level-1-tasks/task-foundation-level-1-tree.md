---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Tree of cubes
## An imaginary tree of cubes

### Task

- Create a tree of cubes. Use cubes of different sizes.

  - Add a cube and scale it down creating a trunk of a tree.
  - Add cubes and scale them to different sizes.
  - Add these cubes as leaves to the tree.
  - [EXTENSION] Add branches with leaves.