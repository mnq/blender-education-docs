---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Modeling Foundation Level 3

<div class="grid cards" markdown>

- **Tools for Editing & Basic Modifiers**

   ---
![](../../resources/img/badges/Modelling%20Level%203.png){align=left width="100"}
This badge covers essential tools for editing and basic modifiers in Blender, including subdivide, loop cut, proportional editing, bevel, and the knife tool. You’ll learn to create complex objects like islands, pirate flags, and coins, master object manipulation, and apply subdivision surface modifier.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Manipulations of mesh objects

   - Set the origin
   - Add and delete edges and faces
   - Ring selection in edit mode
   - Proportional editing
   - Fractal

   Tools in Edit mode

   - Subdivide 
   - Loop cut and slide
   - Bevel 
   - Knife

   Material properties

   - Assign material to designated faces

   Modifiers

   - Subdivision surface

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   Tools in edit mode

   - Subdivide
   - Loop cut & slide
   - Bevel
   - Knife tool
   - Bevel
   - knife

   Materials
   
   - Assign material to faces

   Other

   - Proportional editing

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Use tools in Object and Edit mode.
   - Colour objects and faces.
   - Create different kinds of cuts, different sections
   - Smooth shapes

   **Knows:**

   - Difference between Object and Edit mode.
   - Create a more complex model from a basic shape.
   - Create a more complex model from different complex shapes.

</div>

<div class="grid cards" markdown>

- **Example tasks for Modeling Foundation Level 3**

   ---

   - [Maze](./level-3-tasks/task-foundation-level-3-maze.md)
   - [Landscape](./level-3-tasks/task-foundation-level-3-landscape.md)
   - [Island](./level-3-tasks/task-foundation-level-3-island.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Modeling Foundation Level 3 Badge](../../resources/img/badges/Modelling%20Level%203.png)
     
</div>