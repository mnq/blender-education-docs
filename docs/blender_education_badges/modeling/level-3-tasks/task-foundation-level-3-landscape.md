---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Landscape
## Mountains, trees and a lake

### Task

- Create a landscape with mountains, trees and a lake.

- Basic landscape

  - Add a plane.
  - Resize the plane
  - Subdivide the plane.
  - In edit mode, select vertices and bring them up.
  - Use Proportional Editing to smooth the mountain.
  - Add materials and color the landscape.
  - [EXTENSION] Snow. Give the top of the mountain a white color.

- Trees

  - Add a cylinder
  - Resize to create a tree trunk.
  - Create sections using Loop Cut and Slide.
  - Bend the tree.
  - Add a cylinder.
  - Resize to create a branch.
  - Use Loop Cut and Slide to create sections.
  - Bend the branch.
  - Color the tree trunk.
  - Color the branch.
  - Duplicate the branch.
  - Add a UV sphere.
  - Resize and create leaves.

- Water

  - Select the plane of the landscape.
  - Face select.
  - Select faces of the plane where you want the lake.
  - Pull these faces down.
  - Add a plane.
  - Resize the plane.
  - Place on top of the selected faces.
  - Color the lake.
  - [EXTENSION] Reflections. Add Screen Space Reflections.

- Render

  - Add lights.
  - Set camera.
  - Render image

