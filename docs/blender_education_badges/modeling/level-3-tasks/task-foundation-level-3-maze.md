---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Maze
## Multi-color maze

### Task

- Create a maze.

  - Add a plane.
  - Resize the plane.

- In Edit mode

  - Subdivide the plane.
  - Face select.
  - Select faces and pull them up.

- In Object mode

  - Add material.
  - Color the maze.
  - Add lights.
  - Render the image.