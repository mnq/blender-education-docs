---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Treasure Island
## Island with rocks. palm trees and treasures

### Task

- Create an island. Add some palm trees and rocks. Add a pirate flag and treasures.

- Palm tree

  - Add a cylinder.
  - Scale the cylinder to create the trunk of the palm tree.
  - Make some sections using Loop Cut and Slide tool.
  - Make the bottom of the tree wider.
  - Bend the tree using Edge Loops and Proportional Editing and give it a more organic look.
  - Add a plane.
  - Use the knife tool to cut a palm leaf.
  - Rotate and move some faces to give the leaf a more organic look.
  - Color the palm tree.
  - Color the leaf.
  - Duplicate the leaf.
  - Add the leaves to the palm tree.
  - Join the leaves and tree trunk.

- Island

  - Add a plane.
  - Subdivide the plane
  - Move the faces up and use Proportional Editing for smoothing.
  - Color the island.
  - Duplicate the palm tree and place them on the island.

- Water

  - Add a plane.
  - Subdivide the plane.
  - Set the Fractal.
  - Color the plane.

- Rocks

  - Add a sphere.
  - Resize the sphere.
  - Move vertices to create a nice rock.
  - Color the rock.
  - Duplicate the rock and place them on the island.
  - [EXTENSION] Create different types of rocks.

- Pirate flag
  - Add a cylinder.
  - Resize the cylinder along the z axis.
  - Use Loop cut and Slide to add sections.
  - Use Proportional Editing to make the pole crooked.
  - In edit mode, use ring select and size the base to make it bigger.
  - Add a plane for the flag.
  - Subdivide flag.
  - Make a ragged edge using the knife tool to select and delete faces.
  - Color the flag.

- Coins

  - Add a cylinder.
  - Resize cylinder.
  - Edge select.
  - Bevel edge.
  - Add material.
  - Join objects.
  - Duplicate to create more coins.
  - [EXTENSION] Create a chest and put the coins in the chest.

- Render

  - Add lights.
  - Set camera.
  - Render the image.