---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Snowman
## Color a snowman made from mesh primitives

### Prerequisites

- Blender model of a snowman

### Task

- Color the snowman

  - Load pre-made model of a snowman.
  - Color in the hat.
  - Color in the arm.
  - Reuse color on the second arm.
  - [EXTENSION] Load pre-made trees of cubes and color in the trees.

- Add button, color and duplicate

  - Add/flatten/move/rotate single button.
  - Add color to the button.
  - Duplicate buttons and move them to correct locations.
  - Parent named parts of the snowman together.
  - [EXTENSION] Create trees and parent parts together, duplicate trees to make a forest.

- Build a cabin (give students a model for them to aim at making)

  - Using Edit mode.
  - Select a face and extrude.
  - Use edge select to move and scale parts of the cabin.
  - Select faces to give roof and walls different colors.
  - Return to object mode to position the cabin.
  - Fly through the scene.
  - [EXTENSION] Make a range of different houses and a car. Color in house, parent parts of a house together.
