---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Rocket & Rabbit
## Color the rabbit and add a rocket

### Prerequisites

- Blender pre-made model of a rabbit

### Task

- Color the rabbit

  - Load pre-made model of a rabbit.
  - Color in eyes.
  - Color in ears.
  - Color in legs.
  - Color in tail.
  - Reuse color on legs.
  - Parent named parts of rabbit together.
  - [EXTENSION] Create a lawn and give it a color. Put rabbit on the lawn.

- Build a rocket

  - Add 2 cylinders, scale them and put one on top of the other.
  - Extrude Edges on the bottom creating the rocket fins.
  - Extrude the upper cylinder.
  - Scale the upper cylinder down creating the nose of rocket
  - Color the rocket.
  - [EXTENSION] Give the rocket multiple colors.
