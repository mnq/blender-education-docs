---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Modeling Badges

Modeling in Blender enables artists to create detailed 3D models. Blender provides a variety of editing tools, as well as advanced features like modifiers and procedural modeling with Geometry Nodes, allowing for non-destructive workflows. While these tools are mainly aimed at hard surface modeling, they can also be used for creating organic shapes.

<div class="grid cards" markdown>

- **Foundation badge Level 1**

  ---
  ![](../../resources/img/badges/Modelling%20Level%201.png){align=left width="100"}
  Editing Objects</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-1.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 2**

  ---
  ![](../../resources/img/badges/Modelling%20Level%202.png){align=left width="100"}
  The Basics of Object Manipulation</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-2.md)

</div>
<div class="grid cards" markdown>

- **Foundation badge Level 3**

  ---
  ![](../../resources/img/badges/Modelling%20Level%203.png){align=left width="100"}
  Tools for Editing and Basic Modifiers</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-3.md)

</div>
