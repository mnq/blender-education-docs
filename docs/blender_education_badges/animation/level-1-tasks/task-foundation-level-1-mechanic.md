---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Mechanical animation
## Slam a door or stamp machine

### Prerequisites

- Blender model of a door or stamp machine

### Task

- Create a scene with a door or a stamp machine. 

  - Set the framerate.
  - Set the final frame.
  - Add, move or delete location keyframes to animate the timing and rhythm of the door hitting the frame, bouncing off and finally coming to a full stop.
  - Adjust the keyframes taking spacing into account when the door slams the frame. When does the door pick up speed?
  - Use the Graph Editor to polish the animation by changing the keyframe handles and adding interpolation modes.

- Animate a stamp machine.

  - Set the framerate.
  - Set the final frame.
  - Add, move or delete location keyframes to animate the timing and rhythm of the stamp machine stamping letters.
  - Adjust the keyframes taking spacing into account when the stamp machine hits the letter.
  - Add or adjust keyframes to have the letter being squeezed and stretched.
  - Use the Graph Editor to polish the animation.
