---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Bouncing ball
## Drop a ball on the floor

### Prerequisites

- Blender model of a ball
- Blender model of a floor

### Task

- Create a scene with a ball in the air, above the floor.
- Animate the ball dropping onto the floor.

  - Set the framerate.
  - Set the final frame.
  - Add, move or delete location and rotation keyframes to animate the timing and rhythm of the ball falling on the floor and bouncing coming to a full stop.
  - Adjust the keyframes taking spacing into account when the ball drops on the floor. Think about mass and gravity and whether the ball will drop in a linear way or splined.
  - Add or adjust keyframes and squeeze and stretch the ball when it hits the floor or bounces off.
  - Add Interpolation modes and have the ball follow a nice curve.
  - Use the Graph Editor to polish the animation by changing the keyframe handles and adding interpolation modes.