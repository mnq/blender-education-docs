---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Low poly car race
## Cars are lined up and ready to start a race

### Prerequisites

- Blender models of low poly cars
- Blender model of low poly race track

### Task

- Create a scene with cars on a race track. Animate the start of the car race.

  - Set the framerate.
  - Set the final frame.
  - Add location and rotation keyframes for each car.
  - Keyframes can be deleted or moved.