---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Low poly rabbit jumps onto a rocket
## Low poly rabbit jumps to and into a rocket and flies away

### Prerequisites

- Blender pre-made model of a low poly rabbit
- Blender pre-made model of a rocket

### Task

- Create a scene containing a low poly rabbit and a low poly rocket. 
- Animate the rabbit jumping to and into the rocket. Rocket and rabbit take off and fly away.

  - Set the framerate.
  - Set the final frame.
  - Add location keyframes for rabbit jumping to and into the rocket.
  - Add location keyframes for rocket and rabbit taking off.
  - Add location keyframes for rocket and rabbit flying away.
  - Keyframes can be deleted or moved.