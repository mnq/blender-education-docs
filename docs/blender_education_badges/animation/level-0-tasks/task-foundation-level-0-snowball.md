---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Throwing snowballs
## Throw a snowball at a snowman and let the snowman's head fall off

### Prerequisites

- Blender model of a snowball 
- Blender model of a snowman

### Task

- Create a scene containing a snowball and a snowman. 
- Create an animation where the ball is thrown at the snowman's head. The ball hits the snowman's head and the snowman's head falls off.

  - Set the framerate.
  - Set the final frame.
  - Add location and or rotation keyframes for the snowball.
  - Add location and or rotation keyframes for the snowman's head falling off.
  - Keyframes can be deleted or moved.