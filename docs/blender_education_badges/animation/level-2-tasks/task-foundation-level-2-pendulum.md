---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Pendulum
## Push a pendulum 

### Prerequisites

- Blender model of a pendulum

### Task

- Drag a pendulum to the left or right and animate the motion.

  - Set the framerate.
  - Set the final frame.
  - Add, delete or move keyframes to animate the motion. Take the inertia into account.
  - Use the Graph Editor to finetune the animation.
