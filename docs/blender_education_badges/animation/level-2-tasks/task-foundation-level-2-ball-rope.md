---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Bouncing ball with tail
## Drop a ball with a tail on the floor

### Prerequisites

- Blender model of a ball with a tail
- Blender model of a floor

### Task

- Continue from level 1 task. Add a tail or feather to the ball.
- Animate the tail or feather moving along with the ball. The tail or feather
anticipates to the movement of the ball.

  - Start with the level 1 example.
  - Add and adjust keyframes to have the tail or feather follow the ball.
