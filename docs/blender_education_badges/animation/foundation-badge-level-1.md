---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Animation Foundation Level 1

<div class="grid cards" markdown>

- **Principles of Timing & Rhythm, Spacing, Squash & Stretch and Arcs**

   ---
   ![](../../resources/img/badges/Animation%20Level%201.png){align=left width="100"}
   Learning goal of this badge is to learn how to use some basic principles that will improve your animation. We start with 4 basic principles.

</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Principles

   - Timing
   - Rhythm
   - Spacing
   - Squash & Stretch
   - Arcs

   Graph Editor

   - Keyframe handles

   Interpolation modes

   - Constant
   - Linear
   - Bezier
   - Bounce

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---
   
   - Add, delete or move location, rotation, scale, locrot, locrotscale keyframes.
   - Dope Sheet Context menu: Interpolation modes.
   - Graph Editor
     - Changing keyframe handles
     - Key Frame Interpolation

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Use Graph Editor
   - Add Interpolation modes to animation

   **Knows:**

   - Timing
   - Rhythm
   - Spacing
   - Squash
   - Stretch
   - Arcs

</div>

<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 1**

   ---

   - [Bouncing ball](./level-1-tasks/task-foundation-level-1-ball.md)
   - [Mechanic animation](./level-1-tasks/task-foundation-level-1-mechanic.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Animation Foundation Level 1 Badge](../../resources/img/badges/Animation%20Level%201.png)
     
</div>