---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Car tyre
## Rotating tyre

### Prerequisites

- Blender model of a car with tyres

### Task

- Animate the rotation of the tyres of a car.

  - Set the framerate.
  - Set the final frame.
  - Each x-number of frames, rotate the tyre y-degrees and add keyframes.
  - In the NLA Editor duplicate the strip.