---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Two pendulums
## Two pendulums moving in opposite direction

### Prerequisites

- Blender model of two pendulums

### Task

- Continue with the level 2 task pendulum.
- Add another pendulum and animate the motion.
- Use the NLA Editor for repetitive actions.
