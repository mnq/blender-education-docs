---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Animation Foundation Level 2

<div class="grid cards" markdown>

- **Principles of Anticipation, Drag & Follow through**

   ---
![](../../resources/img/badges/Animation%20Level%202.png){align=left width="100"}
Learning goal of this badge is to apply 2 more principles. For these we will animate multiple objects. It's also important to learn about the difference between Euler and Quaternion rotations as well as the Gimbal lock.

</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---
   Principles

   - Anticipation
   - Drag & Follow through

   Rotation

   - Euler vs Quaternion
   - Gimbal lock

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---
   - Add, delete or move location, rotation, scale, locrot, locrotscale keyframes
   - Graph Editor
   - Changing keyframe handles
   - Keyframe Interpolation

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Use Graph Editor
   - Change keyframe handles
   - Add Interpolation modes to animation

   **Knows:**

   - Anticipation
   - Drag
   - Follow through
   - Euler vs Quaternion rotation
</div>

<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 2**

   ---

   - [Bouncing ball with tail](./level-2-tasks/task-foundation-level-2-ball-rope.md)
   - [Pendulum](./level-2-tasks/task-foundation-level-2-pendulum.md)
</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Animation Foundation Level 2 Badge](../../resources/img/badges/Animation%20Level%202.png)
     
</div>