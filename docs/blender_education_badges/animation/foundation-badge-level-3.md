---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Animation Foundation Level 3

<div class="grid cards" markdown>

- **Actions**

   ---
![](../../resources/img/badges/Animation%20Level%203.png){align=left width="100"}
Learning goal of this badge are Actions and the NLA Editor. How to combine or create repetitive actions for cyclic animations.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---
  Actions

  NLA Editor

  - Repetitive actions (cyclic animation vs repeat)
  - Combining actions

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---
   - Add, delete or move location, rotation, scale, locrot, locrotscale keyframes
   - NLA Editor

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Use Graph Editor
   - Use NLA Editor

   **Knows:**

   - Actions
   - Repeat actions
   - Combine actions
   - Inertia

</div>


<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 3**

   ---
   - [Car tyre](./level-3-tasks/task-foundation-level-3-tyre.md)
   - [Pendulums](./level-3-tasks/task-foundation-level-3-pendulum.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Animation Foundation Level 3 Badge](../../resources/img/badges/Animation%20Level%203.png)
     
</div>