---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Animation Foundation Level 0

<div class="grid cards" markdown>

- **Animating primitive low poly objects**

   ---
   ![](../../resources/img/Animation.png){align=left width="100"}
   Learning goal of this badge is to get a very basic introduction into animation. Student will animate basic, low poly objects or meshes by means of timelines, adding and manipulating keyframes.

</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Setting up the Timeline

   - Frames (Start, End)
   - Framerate
   - Length of the animation

   Keyframing

   - Location
   - Rotation
   - Location/Rotation
   - Location/Rotation

   Manipulating keyframes

   - Add keyframes
   - Delete keyframes
   - Move keyframes

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---
   Some UI tools and keys that will be used

   - Editor Type: Timeline
   - Output Properties tab: Framerate
   - Current, Start and End frame on Timeline
   - Select frame on Timeline
   - I - key or K - key to bring up Insert Keyframe Menu in 3D Viewport and to add a keyframe
   - X - key to delete keyframe on Timeline
   - G - key to move keyframe on Timeline
   - Play, Stop, Go to beginning/end buttons

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Bring up the Insert Keyframe menu
   - Add keyframes (loc/rot/locrot/locrot) to the Timeline
   - Delete keyframes on the Timeline
   - Move keyframes on the Timeline
   - Set the framerate
   - Set Current, Start and End frame on Timeline
   - Playback

   **Knows:**

   - Frames and framerate
   - Movement by means of changing location and rotation over time
   - Movement has a start, an end and steps in between

</div>

<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 0**

   ---
   - [Throwing a snowball](./level-0-tasks/task-foundation-level-0-snowball.md)
   - [Fly away rabbit](./level-0-tasks/task-foundation-level-0-rabbit.md)
   - [Car race](./level-0-tasks/task-foundation-level-0-car-race.md)
   
  
</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Animation Foundation Level 0 Badge](../../resources/img/Animation.png)
     
</div>
