---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Animation Badges

Creating the illusion of movement.<br>
To get a basic understanding of animation we have set up 4 basic foundation badges that address these fundamentals and how this is done in Blender.

<div class="grid cards" markdown>

- **Foundation badge Level 0**

  ---
  ![](../../resources/img/Animation.png){align=left width="100"}
  Animating primitive low poly objects</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-0.md)

</div>
<div class="grid cards" markdown>

- **Foundation badge Level 1**

  ---
  ![](../../resources/img/badges/Animation%20Level%201.png){align=left width="100"}
  Principles of Timing & Rhythm, Spacing, Squash & Stretch, Arcs</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-1.md)

  </div>

<div class="grid cards" markdown>

- **Foundation badge Level 2**

  ---
  ![](../../resources/img/badges/Animation%20Level%202.png){align=left width="100"}
  Principles of Anticipation, Drag & Follow through</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-2.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 3**

  ---
  ![](../../resources/img/badges/Animation%20Level%203.png){align=left width="100"}
  Create and combine Blender Actions</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-3.md)

</div>