---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Shading, Texturing UV Foundation level 2

<div class="grid cards" markdown>

- **Shading, Texturing UV**

  ---
![](../../resources/img/badges/Shading%20Level%202.png){align=left width="100"}
The challenge here centers on mastering UV unwrapping techniques, including the use of seams and islands, as well as scaling UVs with a checker pattern for accuracy. Alongside UV mapping, this badge introduces understanding the flow of shaders in the shader editor, exploring the difference between textures and shaders, and utilizing texture coordinates and mapping nodes.

</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   UV Unwrapping

   - Different Unwrapping methods
   - UV scale ( checker pattern)
   - Seams & Islands
   - Correct face attribute

   Shading

   - Shader Editor:

     - What is a shader
     - Understanding the flow of a shader
     - Shader vs Texture
     - Image texture vs Procedural texture
     - Texture Coordinates + Mapping node


</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - Uv Sync Selection
   - U (Unwrap)
   - Select an edge, Rightclick mark seam.

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Mark seams to object
   - Unwrap the barrel
   - Assign the texture to the barrel

</div>

<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 0**

   ---

  - [Barrel](./level-2-tasks/task-foundation-level-2-barrel.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Shading, Texturing UV Foundation Level 2 Badge](../../resources/img/badges/Shading%20Level%202.png)
     
</div>