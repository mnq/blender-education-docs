---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Shading, Texturing UV Foundation level 1

<div class="grid cards" markdown>

- **Shading, Texturing UV**

  ---
![](../../resources/img/badges/Shading%20Level%201.png){align=left width="100"}
This badge focuses on the basics of applying materials to objects, starting with adjusting key properties like diffuse color, roughness, metallic, and specular values. It involves assigning different materials to individual faces of a model and incorporating texture-based colors to enhance the realism of the object.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Material: Coloring objects

   - Diffuse (Base Color) material
   - Roughness, Metallic, Specular
   - Applying materials to different faces
   - Assigning material to faces

   Shading:

  - Texture based color.

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

  Material Properties tab

  - Preview
  - Adding/ deleting new material
  - Assigning new material

  PBR shader

  - values

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Create | delete a new material
   - Apply material to an object
   - Assign material to a face

   **Knows:**

   - Basic understanding of materials and shading.

</div>

<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 0**

   ---

   Making a Color Dice.

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Shading, Texturing UV Foundation Level 1 Badge](../../resources/img/badges/Shading%20Level%201.png)
     
</div>