---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Shading, Texturing UV Foundation level 3

<div class="grid cards" markdown>

- **Shading, Texturing UV**

  ---
![](../../resources/img/badges/Shading%20Level%203.png){align=left width="100"}
This badge delves into advanced shader nodes such as Noise, Voronoi, and Mix Shader/Mix RGB, with an emphasis on manipulating these nodes through input factors and separate XYZ components. Additionally, the challenge includes texture painting with brushes and stencils, saving custom textures, and improving efficiency using the Node Wrangler addon to manage existing textures effectively.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Shader nodes

   - Noise texture
   - Voronoi
   - Mix Shader vs Mix Rgb
   - Input Factor
   - Separate xyz node

   Texture painting

   - Texture painting brushes
   - Using texture as stencil
   - Saving your Texture paints

   Node Wrangler

   - Working with existing textures


</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   Node wrangler

   - Ctrl + T (add mapping setup)
   - Ctrl + Shift T ( add pbr shader setup)
   - Shift + right click -> reroute

   In UV image editor

   - Saving your texture

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Texture paint an object
   - Create a new empty texture
   - Load created texture in shader
   - Mix two materials in a shader

   **Knows:**

   - How to load a texture in the brush

</div>

<div class="grid cards" markdown>

- **Example tasks for Animation Foundation Level 0**

   ---

  - [Fruit](./level-3-tasks/task-foundation-level-3-fruit.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Shading, Texturing UV Foundation Level 3 Badge](../../resources/img/badges/Shading%20Level%203.png)
     
</div>