---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Barrel
## Design & texture a Barrel

### Task

- Creating the barrel

  - Add a cylinder (shift + A).
  - Loopcut add subdivide where the metal rings are.
  - Loopselect (alt/option + shift +click) and E extrude the rings.

- Uv Unwrapping

  - With edge selection select where the seams should be.
  - Right click add seams.
  - U - Unwrap.

- Shader Editor

  - Add an image texture and connect this to the base color.
  - Add a texture coordinate and a mapping node, connect the texture coordinate UV output to the mapping vertex input to the image texture, Your texture is now listening to your objects UVs.
  - Open a wood texture in the image texture, you should now see your texture in the texture preview mode on your model.
  - Duplicate your image texture, open now the roughness, and connect this to the roughness input.
  - Duplicate your image texture and open the normal map, connect this to a normal map node and plug this in to the normal map input in your principled shader.
