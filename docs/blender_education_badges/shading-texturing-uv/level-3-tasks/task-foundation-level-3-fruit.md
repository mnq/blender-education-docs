---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Fruit
## Texture paint a piece of fruit

### Task

- Create a piece of fruit with a nice quad topology that is not too dense.
- Uv unwrap your piece of fruit. Create a seam if necessary (see level 2)

- In the Image editor

  - Create a new image texture with a 2048 x 2048 resolution. Call it texture paint. You should now see your UV's on top of a black image, If not make sure UV sync Selection is enabled.

- Shader editor

  - Create an image texture and plug it in the base color input.
  - Select your "texture paint" texture.

- Texture paint window

  - You can now draw on top of your object.
  - In the brush panel.
