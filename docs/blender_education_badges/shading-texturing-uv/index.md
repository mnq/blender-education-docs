---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Shading and Texturing UV Badges

 Shading, texturing, and UV-editing in Blender provide artists with the ability to add realistic surface details and materials to 3D models. Blender’s shader editor allows for the creation of complex materials using a node-based system, while the texturing process adds visual details like colors, patterns, and imperfections. UV mapping enables the precise placement of textures by flattening 3D surfaces into a 2D space, offering full control over how textures wrap around a model’s geometry.

<div class="grid cards" markdown>

- **Foundation badge Level 1**

  ---
  ![](../../resources/img/badges/Shading%20Level%201.png){align=left width="100"}
  Basic Material Application and Texture Setup</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-1.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 2**

  ---
  ![](../../resources/img/badges/Shading%20Level%202.png){align=left width="100"}
  UV Mapping and Shader Fundamentals</br>
  </br>
  [:octicons-arrow-right-24:  View more](foundation-badge-level-2.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 3**

  ---
  ![](../../resources/img/badges/Shading%20Level%203.png){align=left width="100"}
  Advanced Shader Nodes and Texture Painting</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-3.md)

</div>
