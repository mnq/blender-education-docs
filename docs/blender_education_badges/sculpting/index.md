---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Sculpting Badges

Sculpting in Blender allows artists to create detailed 3D models by forming objects in an intuitive and organic way, as if using a digital version of clay. Blender offers various tools for shaping, texturing, and refining models with precision, combining traditional sculpting techniques with digital features like dynamic topology and multi-resolution sculpting. It is often used for modeling organic shapes, although it is perfectly suitable and offers valuable tools for modeling hard surfaces as well.


<div class="grid cards" markdown>

- **Foundation badge Level 1**

  ---
  ![](../../resources/img/badges/Sculpting%20Level%201.png){align=left width="100"}
  Using the Graphic Tablet, Brushes, and Remeshing</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-1.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 2**

  ---
  ![](../../resources/img/badges/Sculpting%20Level%202.png){align=left width="100"}
  Dynamic Topology and Masking</br>
  </br>
  [:octicons-arrow-right-24: View More](foundation-badge-level-2.md)

</div>


<div class="grid cards" markdown>

- **Foundation badge Level 3**

  ---
  ![](../../resources/img/badges/Sculpting%20Level%203.png){align=left width="100"}
  Multiresolution modifier and custom brushes</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-3.md)

  </div>
