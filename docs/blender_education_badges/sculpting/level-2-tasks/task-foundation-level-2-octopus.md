---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Octopus
## A stylized octopus

### Task

- Start with a sphere in sculpt mode.
- Enable Dyntopo and set the detail size to 4px.
- Use the snake hook (k) to extrude out 8 octopus arms from the sphere.
- Use the inflate brush (i, and holding ctrl for inversion) to inflate the arms if needed, and to create the suckers.
- Use masking to support working on the head, eyes, and details.