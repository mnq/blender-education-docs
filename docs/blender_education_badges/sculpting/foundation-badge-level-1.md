---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Sculpting Foundation Level 1

<div class="grid cards" markdown>

- **Using the Graphic Tablet, Brushes, and Remeshing**

   ---
![](../../resources/img/badges/Sculpting%20Level%201.png){align=left width="100"}
Sculpting becomes much easier when using a graphic tablet instead of a mouse, so getting familiar with this tool first makes perfect sense.<br>
Thereafter, learning how to adjust the density of the model's mesh with the Remesh option is the logical next step, since sculpting requires sufficient topology to work on the details.
Finally, using and adjusting the main sculpting brushes should not be missing from this introduction.


</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   - Setting up the graphic tablet
   - Navigating in sculpt mode with the pen

   Using the sculpt brushes

   - Adjusting the brush size
   - Adjusting the brush strength
   - Draw
   - Grab
   - Smooth
   - Inverting brush effect (add / subtract)

   Adjusting the mesh density (Remesh)

   - Why does mesh density matter?
   - Setting voxel size
   - Applying voxel size to mesh


</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - f: adjust brush size
   - SHIFT + f: adjust brush strength

   - x: draw (v since 4.0)
   - g: grab
   - SHIFT + s: smooth (s since 4.0)
   - hold CTRL while drawing: invert brush effect

   - r: set voxel size (shift + r in earlier versions)
   - CTRL +r : apply voxel size to mesh

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Use the graphic tablet
   - Adjust sculpt brush size and strength
   - Set voxel size for mesh density
   - Apply voxel size to mesh
   - Use the draw brush
   - Use the grab brush
   - Use the smooth brush
   - Invert the effect of a brush

   **Knows:**

  - Level of details depends on mesh density

</div>


<div class="grid cards" markdown>

- **Example tasks for Sculpting Foundation Level 1**

   ---

   - [Rock](./level-1-tasks/task-foundation-level-1-sculpt-rock.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Sculpting Foundation Level 1 Badge](../../resources/img/badges/Sculpting%20Level%201.png)
     
</div>
