---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Sculpting Foundation Level 2

<div class="grid cards" markdown>

- **Dynamic Topology and Masking**

   ---
   ![](../../resources/img/badges/Sculpting%20Level%202.png){align=left width="100"}
   While remeshing is a good way to control the scope of the topology in the model, it has its limitations when it comes to adding topology to specific parts of the model.
   This is where dynamic topology comes into play, which makes it possible to intuitively extend the model where needed.
   This is also where masking becomes more important to temporarily separate the parts that are to be modeled from those that should not be affected.


</div>
<div class="grid cards" markdown>


- **What a student will learn**

   ---

   Dynamic Topology (Dyntopo)

   - Adjusting detail size
   - Set refine method
   - Set detailing

   Understanding differences between Dyntopo and Remesh

   - Pros/cons and use cases for Dyntopo
   - Pros/cons and use cases for Remesh


   Using more sculpt brushes

   - Snake hook, flatten, inflate

   Masking

   - Why masking?
   - Drawing mask
   - Inverting mask
   - Clearing mask


</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - r: adjust detail size (when Dyntopo is checked)

   - SHIFT + t: flatten
   - p: pinch
   - k: snake hook

   - shift + space: context menu

   - SHIFT + CTRL + lmb: lasso mask (gone in 4.0)
   - CTRL i: invert mask
   - ALT + m: clear mask
   - a: mask pie menu

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

  **Can:**

  - Extrude organic shapes from simple mesh
  - Create, invert, and clear mask
  - Use the flatten brush
  - Use the snake hook brush
  - Select brushes from the context menu

  **Knows:**

  - Difference between re-mesh (refining the mesh) and Dyntopo (adding geometry while sculpting)
  - Difference between Dyntopo refine methods
  - Difference between Dyntopo detailing settings

</div>

<div class="grid cards" markdown>

- **Example tasks for Sculpting Foundation Level 2**

   ---

   - [Octopus](./level-2-tasks/task-foundation-level-2-octopus.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Sculpting Foundation Level 2 Badge](../../resources/img/badges/Sculpting%20Level%202.png)
     
</div>