---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Voronoi
## Create a sphere with a Voronoi surface

### Task

- Start with an Ico Sphere.
- Add the Multiresolution modifier to the sphere.
- Click on "Subdivide" to add subdivisions using the Catmull-Clark-algorithm, repeat until 6 levels are reached.
- Add a new Voronoi texture to the draw brush.
- To be continued...