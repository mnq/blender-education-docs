---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Sculpt a stone
## Sculpt a rock out of the default cube.

### Task

- Start with the default cube.
- Remesh (r, ctrl+r) cube to add geometry.
- Use the draw (x, ctrl+x), grab (g), and smooth (s) tools to form a stone, adjust the brush size (f) and strength (shift+f) for fine grained sculpting.
- Refine the surface by remeshing and smoothing.
- Repeat c. and d. until you are satisfied with the outcome.
