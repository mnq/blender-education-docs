---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Sculpting Foundation Level 3

<div class="grid cards" markdown>

- **The Multiresolution Modifier**

   ---
   ![](../../resources/img/badges/Sculpting%20Level%203.png){align=left width="100"}
   As the creative process progresses, it becomes more important to be able to switch between different levels of detail in the model. This can be achieved with the Multiresolution Modifier which, as its name suggests, allows for switching between different resolutions of the model's mesh.

</div>
<div class="grid cards" markdown>


- **What a student will learn**

   ---

   Using the multiresolution modifier

   Using textures

   Settings

   - Brush
   - Texture
   - Stroke
   - Falloff
   - Cursor

   Creating custom brushes

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - Modifier Properties:
     - Add Modifier > Generate > Multiresolution

   - Texture Properties
     - New  > Type: Voronoi

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Knows:**

   - The effect of different Texture Mapping options
   - The effect of different Stroke Method options


</div>

<div class="grid cards" markdown>

- **Example tasks for Sculpting Foundation Level 3**

   ---

   - [Voronoi](./level-3-tasks/task-foundation-level-3-voronoi.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Sculpting Foundation Level 3 Badge](../../resources/img/badges/Sculpting%20Level%203.png)
     
</div>