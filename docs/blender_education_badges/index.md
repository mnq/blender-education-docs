---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# The Education Badges Project

![](../resources/img/infographic.png){align=left width="350"}

The goal of the Education Badges project is to create a reference framework for common understanding and to improve collaboration that still allows everyone to approach Blender in their own way.<br>

A shared standard helps educators to align and compare their teaching approaches and course content, providing clarity on topics like beginner or advanced modeling, and improves collaboration between schools.

The reference framework also comes with badges. For students, badges serve as a way to communicate the skills they have learned, facilitating understanding between schools, educators, and in contexts like student exchange programs.

Educators can decide how to integrate badges into their curriculum, simply using them as guidelines to prepare their learning materials and/or awarding them based on the skills their students have mastered.

The project started by identifying fundamentals. These fundamentals are organized into Foundation levels. As follow up on the Foundation levels there will be Professional levels.

The project is still under development. If you would like to contribute, please visit the [Get Involved](./../get_involved/index.md).

<div class="grid cards" markdown>

- **Modelling**

  ---
![](../resources/img/Modelling.png){align=left width="100"}
Fundamentals of 3D modeling in Blender, including essential techniques for creating and texturing basic objects and environments.
<br>
<br>
[:octicons-arrow-right-24: View more](modeling/index.md)

</div>

<div class="grid cards" markdown>

- **Animation**

  ---
![](../resources/img/Animation.png){align=left width="100"}
Essentials of animation in Blender, including keyframing, timelines, interpolation modes , repetitive or cyclic actions, graph editor, dope sheet.
<br>
<br>
[:octicons-arrow-right-24: View more](animation/index.md)

</div>

<div class="grid cards" markdown>

- **Sculpting**

  ---
![](../resources/img/Sculpting.png){align=left width="100"}
Digital sculpting in Blender, introducing tools and techniques to create organic 3D models.<br><br>[:octicons-arrow-right-24: View more](sculpting/index.md)

</div>

<div class="grid cards" markdown>

- **Rigging**

  ---
![](../resources/img/Rigging.png){align=left width="100"}
Principles of rigging in Blender, including the creation of skeletons and control systems for animating characters and objects.<br><br>[:octicons-arrow-right-24: View more](rigging/index.md)

</div>

<div class="grid cards" markdown>

- **Shading, Texturing UV**

  ---
![](../resources/img/Shading.png){align=left width="100"}
Shading, texturing, and UV mapping in Blender to enhance models with surfaces and materials.<br><br>[:octicons-arrow-right-24: View more](shading-texturing-uv/index.md)

</div>

<div class="grid cards" markdown>

- **Lighting, Camera and Rendering**

  ---
![](../resources/img/Render.png){align=left width="100"}
Use of lighting, camera setups, and rendering techniques in Blender to produce images and videos.<br><br>[:octicons-arrow-right-24: View more](lighting-camera-rendering/index.md)

</div>
