---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Lighting, Camera and Rendering Foundation level 3

<div class="grid cards" markdown>

- **Lighting Techniques and Camera Effects**

  ---
![](../../resources/img/badges/Render%20Level%203.png){align=left width="100"}
Grasp popular techniques like the 3-point lights system and camera depth of field to enhance your creations.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Light setup

   - Basic 3-point lighting: Key, Bounce and Rim

   Camera

   - DOF

   Cycles rendering

   - Output settings: Resolution, Output

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - Light Property menu
   - Camera Property menu
   - Render menu + shortcuts
   - Render settings

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Setup a 3-point lighting: Key, Bounce and Rim

   **Knows:**

   - Difference between different types of light
   - DOF
   - Cycles render output settings

</div>

<div class="grid cards" markdown>

- **Example tasks for Lighting, Camera and Rendering Foundation level 3**

   ---

   - [Character](./level-3-tasks/task-foundation-level-3-character.md)
</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Lighting, Camera and Rendering Foundation Level 3 Badge](../../resources/img/badges/Render%20Level%203.png)
     
</div>