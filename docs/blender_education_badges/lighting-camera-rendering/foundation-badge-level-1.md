---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Lighting, Camera and Rendering Foundation level 1

<div class="grid cards" markdown>

- **Basic Lighting and Rendering with EEVEE**

  ---
![](../../resources/img/badges/Render%20Level%201.png){align=left width="100"}
Learn to create and move lights, change their colors and strength, and to create cameras to customize your scene's look and feel. Also you will learn to render your scene as an image using Blender realtime render engine EEVEE.
</div>

<div class="grid cards" markdown>

- **What a student will learn**

   ---

   Light Object Data (Point)

   - Transform
   - Power
   - Color

   Camera

   - Camera view
   - Camera to view

   EVEE rendering

   - Viewport Shading Rendered
   - Render image

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - Light Property menu
   - Camera Property menu
   - Render menu + shortcuts
   - Render settings

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Add light to the scene
   - Position light properly in your scene
   - Change color of the light

   **Knows:**

   - Point lights
   - Light color and intensity
   - Transform lights

</div>

<div class="grid cards" markdown>

- **Example tasks for Lighting, Camera and Rendering Foundation level 1**

   ---

   - [Day lights](./level-1-tasks/task-foundation-level-1-point.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Lighting, Camera and Rendering Foundation Level 1 Badge](../../resources/img/badges/Render%20Level%201.png)
     
</div>