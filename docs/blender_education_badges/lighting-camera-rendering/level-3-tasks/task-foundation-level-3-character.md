---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Character lighting
## Light a character using 3-point lighting

### Prerequisites

- Blender character

### Task

- Create a simple 3-point lighting for a character.