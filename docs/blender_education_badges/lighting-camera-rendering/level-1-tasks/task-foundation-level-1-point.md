---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Types of daylight
## Create lighting for dawn, midday and dusk

### Prerequisites

- Blender scene that needs lighting

### Task

- Create a point light for dawn
- Change  the light color to orange and position it behind the fire hydrant.
- Render the image and hide the light
- Create a point light for midday
- Change the light color to yellow and position it above the fire hydrant.
- Render the image and hide the light
- Create a point light for dusk
- Change the light color to purple and position it in front of the fire hydrant.
- Render the image

Examples

![](../../../resources/img/dawn.png){align=left width="200"}
![](../../../resources/img/midday.png){align=left width="200"}
![](../../../resources/img/dusk.png){align=left width="200"}