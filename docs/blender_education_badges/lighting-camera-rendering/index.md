---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Lighting, Camera and Rendering Badges

 With lighting, cameras and rendering you can create lights to set the mood of your scene, add cameras to decide the point of view of the audience and render to generate images or videos.

<div class="grid cards" markdown>

- **Foundation badge Level 1**

  ---
  ![](../../resources/img/badges/Render%20Level%201.png){align=left width="100"}
  Basic Lighting and Rendering with EEVEE</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-1.md)

</div>
<div class="grid cards" markdown>

- **Foundation badge Level 2**

  ---
  ![](../../resources/img/badges/Render%20Level%202.png){align=left width="100"}
  Advanced Lighting and Cycles Rendering</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-2.md)

</div>

<div class="grid cards" markdown>

- **Foundation badge Level 3**

  ---
  ![](../../resources/img/badges/Render%20Level%203.png){align=left width="100"}
  Lighting Techniques and Camera Effects</br>
  </br>
  [:octicons-arrow-right-24: View more](foundation-badge-level-3.md)

</div>
