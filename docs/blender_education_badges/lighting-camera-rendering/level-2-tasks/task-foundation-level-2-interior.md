---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Interior desk lighting
## Create three different lighting for an interior desk

### Prerequisites

- Blender scene of an interior with a desk

### Task

- Sun light from the window
- table light using the spot light
- ceiling light using the area light