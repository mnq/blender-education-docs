---
hide:
  #- navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Lighting, Camera and Rendering Foundation level 2

<div class="grid cards" markdown>

- **Advanced Lighting and Cycles Rendering**

  ---
![](../../resources/img/badges/Render%20Level%202.png){align=left width="100"}
Take control of the other type of lights to create different lighting styles, from a spot light to the natural sunlight. In addition, start with the powerful render engine Cycles to get even more beautiful visuals.
</div>

<div class="grid cards" markdown>
- **What a student will learn**

   ---

   Types of light

   - Sun
   - Spot
   - Area
   - Point

   Camera

   - Focal length

   Cycles rendering

   - Sampling

</div>

<div class="grid cards" markdown>

- **Interface / Keyboard**

   ---

   - Light Property menu
   - Camera Property menu
   - Render menu + shortcuts
   - Render settings

</div>

<div class="grid cards" markdown>

- **Ticklist**

   ---

   **Can:**

   - Add different types of light to the scene
   - Combine different types of light in a scene
   - Change focal length of a camera
   - Can adjust the sampling in Cycles rendering

   **Knows:**

   - Difference between different types of light
   - Focal length
   - Sampling

</div>

<div class="grid cards" markdown>

- **Example tasks for Lighting, Camera and Rendering Foundation level 2**

   ---

   - [Interior desk](./level-2-tasks/task-foundation-level-2-interior.md)

</div>

<div class="grid cards" markdown>

- **Badge**

   ---
   [Lighting, Camera and Rendering Foundation Level 2 Badge](../../resources/img/badges/Render%20Level%202.png)
     
</div>