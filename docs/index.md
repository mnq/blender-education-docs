---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Blender Education

This is a community of teachers who love to teach 3D stuff!

Blender is a fantastic tool for many teachers in many areas. The Blender Education Community strives to make Blender more acessible for teachers and students.

By being free and open-source, Blender ensures that teachers and students have unrestricted access to high-quality tools without financial barriers or privacy concerns.

The Blender Education Community shares resources, best practices, and teaching methods, fostering mutual support among educators. This creates an environment where knowledge and experiences are exchanged, continuously improving the quality of education.

Recurring meetings are held at the Blender Conference, where members present their work, share insights, and discuss community projects.

<div class="grid cards" markdown>

- **The Blender Education Badges Project**

  ---
![](resources/img/Animation.png){align=left width="100"}
The Blender Education Badges project is a community project to create a reference framework including badges for teaching Blender. Using this reference framework teachers can communicate more specific about what they teach.<br><br>[:octicons-arrow-right-24: View more](blender_education_badges/index.md)

</div>

<div class="grid cards" markdown>

- **Education Website**

  ---
This education section on the Blender website is maintained by the Blender Education Community. It is a space for sharing community project deliverables and other content.</br>
The Blender Badges and their educational material are licensed under [CC0 4.0](https://creativecommons.org/public-domain/cc0/), or any later version.</br></br>
Reach out to members of this community if you have any questions or want to get involved. You can join the [Blender Education chat](https://chat.blender.org/#/room/#blender-education:blender.org) or visit the [Get Involved](./get_involved/index.md).

</div>
