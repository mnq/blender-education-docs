def on_page_markdown(markdown, page, **kwargs):
    '''Avoid warnings regarding folder links on naviagtion.md pages'''

    if page.file.src_path.endswith("/navigation.md"):
        return ""
    else:
        return markdown