# Stage 1: Build the static site
FROM python:3.10-slim AS builder

# Set the working directory
WORKDIR /app

# Copy the project files into the container
COPY . .

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Build the MkDocs static site
RUN mkdocs build

# Stage 2: Serve the static site using Nginx
FROM nginx:alpine

# Copy the built site from the builder stage to the Nginx HTML directory
COPY --from=builder /app/site /usr/share/nginx/html

# Expose port 80 for the web server
EXPOSE 80

# Start Nginx
CMD ["nginx", "-g", "daemon off;"]
